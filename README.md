# Debug_utils


## Cluster Autoscaling

Sscripts to get hold of relevant logs relevant to cluster autoscaling.

#### login to nandi

```bash
$ nandi
```

#### login to one of the web nodes

```bash
$ qds-ops -e production web login
```

_NB: these are production web node. DO NOT DO HEAVY LIFTING. Unfortunately there is no other place to go do this analysis other than copying entire customer log to personal bucket which may be breach !!_

#### Find out the default log location

```bash
$ cd src/qbol/tapp
$ rails c qubole
```

```rails
>>> ci = ClusterInst.find(1810608); file_path = "s3://#{ci.cluster.account.defloc}/logs/hadoop/#{ci.cluster.id}/#{ci.id}/#{ci.cluster_nodes.select{|a| a.role == 'master'}[0].try(:hostname)}/" ; puts "/usr/lib/hive_scripts/storagecli.py --account-id=#{ci.cluster.account.id} --cmd=\"-ls #{file_path}\""
```

This should give you def log.

#### Fetch logs

##### make directory

Make directory where you want to store the logs. Preferably inside ~/<your username> as the logs may big in certain cases. 

```bash
$ mkdir <cluster id>
```

##### find master node

```bash
$ /usr/lib/hive_scripts/storagecli.py --account-id=4098 --cmd="-ls s3://demandbase-data-engineering/qubole/logs/hadoop/69645/1813947/"
```

##### yarn RM logs
```bash
$ /usr/lib/hive_scripts/storagecli.py --account-id=4098 --cmd="-CopyToLocal s3://demandbase-data-engineering/qubole/logs/hadoop/69645/1813947/ec2-54-193-120-101.us-west-1.compute.amazonaws.com.master/yarn ."
```


##### driver logs
```bash
/usr/lib/hive_scripts/storagecli.py --account-id=4098 --cmd="-CopyToLocal s3://demandbase-data-engineering/qubole/logs/hadoop/69645/1813947/ec2-54-193-120-101.us-west-1.compute.amazonaws.com.master/zeppelin/logs ."
```

##### process logs

Switch to parent directory. Copy both the scripts summary.sh and cost.py in this directory and Run

```bash
$ ./summary.sh <cluster id>
```

This command does following:

1. Parses log file into LOG_<cluster_id> with relevant logs
2. Parses log file into EVENTLOG_<cluster id> with yarn and driver logs merged
3. Dump summary information in terminal

##### Output

```bash
******************************  1810608 ****************************************
-------------------------------------------------------------------------
Containers per node:             4                                          ==> Config
-------------------------------------------------------------------------
Total Unique Containers :        252                                        ==> Unique containers used by spark job
Total Unique Nodes:              212                                        ==> Number of unique nodes added by Yarn
-------------------------------------------------------------------------
Max containers [nodes used * max containers per node]: 180                  ==> Maximum container at any point in time
Unsed nodes count: 55 Cost Hours: 52.2983363889                             ==> Unused node count and cost of these nodes.
Used nodes count: 45                                                        ==> Used node count
-------------------------------------------------------------------------
```


