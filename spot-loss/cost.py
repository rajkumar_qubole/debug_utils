#!/usr/bin/python

from dateutil.parser import parse
from datetime import timedelta

cost = dict()
assigned = dict()
added = dict()

remove = dict()

def timedelta_total_seconds(timedelta):
    return (
        timedelta.microseconds + 0.0 +
        (timedelta.seconds + timedelta.days * 24 * 3600) * 10 ** 6) / 10 ** 6

def processfile(fp):
    line = fp.readline()
    cnt = 1

    fields = line.split()
    node = fields[0]
    time = fields[1]
    action = fields[2]

    while line:
        fields = line.split()
        node = fields[0]
        time = fields[1]
        action = fields[2]

        if action == "Assigned":
            assigned[node] = "Assigned"

        if action == "Removed":
            if node in added.keys() and node not in assigned.keys():
                d1 = parse(added[node])
                d2 = parse(time)
                delta = d2 - d1
                if node in cost.keys():
                    cost[node] += timedelta_total_seconds(delta)
                else:
                    cost[node] = timedelta_total_seconds(delta)
                added.pop(node, None)

        if action == "Added":
            added[node] = time

        line = fp.readline()
        cnt += 1

    # for all remaining nodes last removed is considered removed time
    # for all those added nodes which are note  removed yet.
    d2 = parse(time)
    for k,v in added.iteritems():
        node = k
        if node in assigned.keys():
            continue;

        d1 = parse(v)

        delta = d2 - d1
        if node in cost.keys():
            cost[node] += timedelta_total_seconds(delta)
        else:
            cost[node] = timedelta_total_seconds(delta)

import sys
try:
    fp = open(sys.argv[1])
    # do stuff
    processfile(fp)
finally:
    fp.close()

    total_cost =0
    cnt= 0
    for k,v in cost.iteritems():
        cnt += 1
        total_cost += v

    print "Unsed nodes count: " + str(cnt) + " Cost Hours: " + str(total_cost/3600)

    cnt = 0
    for k in assigned:
        if k not in cost.keys():
            cnt += 1

    print "Used nodes count: " + str(cnt)
