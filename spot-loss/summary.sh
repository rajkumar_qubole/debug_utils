#!/bin/bash

## Utility file to fetch spot loss related details from the logs.
##
## Summary Output
##  App ID  | Application Status | Number of Spot Loss | Number of Stage Failure Ignored  |  Number of Task Failure Due to Spot loss
##
## Spot loss time line
##  Node    | Spot Loss Time Line


