#!/bin/bash

parselog () {
	FCNT=$(ls ${1}/yarn/yarn-yarn-resourcemanager* | wc -l)
	if [[ ${FCNT} -gt 1 ]]
	then
       		zgrep -i "assigned container.*on host\|released container .* on host\|upscale\|added node\|Total resources requested\|Total estimated node\|getTotalPendingRequests report\|current cluster size" ${1}/yarn/* | cut -d ':' -f 2- > TMP_LOG
	else
       		zgrep -i "assigned container.*on host\|released container .* on host\|upscale\|added node\|Total resources requested\|Total estimated node\|getTotalPendingRequests report\|current cluster size" ${1}/yarn/* > TMP_LOG
	fi
       	if [[ -e ${1}/logs ]]
	then
		FCNT=$(ls ${1}/logs/zeppelin-interpr* | wc -l)
		if [[ ${FCNT} -gt 1 ]]
		then
		       	zgrep -i "executor.cores=\|quboleallocationstrategy\|ExecutorAllocationManager" ${1}/logs/zeppelin-interpre* | cut -d ':' -f 2- >> TMP_LOG
		else
       			zgrep -i "executor.cores=\|quboleallocationstrategy\|ExecutorAllocationManager" ${1}/logs/zeppelin-interpre* >> TMP_LOG
		fi
	else
		echo "No Zeppelin directory for driver log"
	fi

       	sed -i -r 's/([0-9]+)\/([0-9]+)\/([0-9]+)/20\1-\2-\3/' TMP_LOG
       	sort TMP_LOG > LOG_${1}
}

summary() {
        NODES=$(zgrep -i "added node"  LOG_${1} | cut -d ' ' -f 7 | sort -u | wc -l)
        CONTAINERS=$(zgrep -i "assigned container"  LOG_${1} | wc -l)
        RELEASED_CONTAINERS=$(zgrep -i "assigned container"  LOG_${1} | wc -l)
        CONTAINER_NODES=$(zgrep -i "assigned container" LOG_${1} | cut -d ' ' -f 14  | sort -u | wc -l)
	MAX_CONTAINER_PER_NODE=$(zgrep -i "assigned container" LOG_${1} |cut -d ',' -f 4 | cut -d' ' -f 4 | sort -u | tail -1)
	MAX_CONTAINERS=$(( ${MAX_CONTAINER_PER_NODE} * ${CONTAINER_NODES} ))
	CORES_PER_EXECUTOR=$(zgrep -i "cores=" LOG_${1} | tail -1 | cut -d '=' -f 2)
	MAX_EXECUTORS=$(zgrep -i "new total is" LOG_${1} | cut -d '(' -f2 | cut -d ' ' -f 4 | cut -d ')' -f 1 | sort -un | tail -1)

	echo "-------------------------------------------------------------------------"
	echo "Containers per node:             ${MAX_CONTAINER_PER_NODE}"
	if [[ -e ${CORES_PER_EXECUTOR} ]]
	then
		echo "Core per executor:               ${CORES_PER_EXECUTOR}"
	fi
	echo "-------------------------------------------------------------------------"
	echo "Total Unique Containers :        ${CONTAINERS}"
	echo "Total Unique Nodes:              ${NODES}"
	echo "-------------------------------------------------------------------------"
	echo "Max containers [nodes used * max containers per node]: ${MAX_CONTAINERS}"
	if [[ -e ${CORES_PER_EXECUTOR} ]]
	then
		echo "Max executors:                   ${MAX_EXECUTORS}"
		echo "Max tasks [max containers * 2]:  $(( ${MAX_CONTAINERS} * ${CORES_PER_EXECUTOR} ))"
	fi
	cost_summary $@
	echo "-------------------------------------------------------------------------"

}

cost_summary() {
	zgrep -i "Added node\|Removed node" ${1}/yarn/yarn-yarn-resource* | cut -d ':' -f 2-5 | awk '{print $7" "$1"T"$2" "$5}' > TMP_LOG
	zgrep -i "assigned container\|released container" ${1}/yarn/yarn-yarn-resource* | grep "SchedulerNode" | cut -d':' -f 2,3,4,7,5 | awk '{print $13" "$1"T"$2" "$5}' >> TMP_LOG
	sort TMP_LOG > COST_LOG
	python cost.py COST_LOG
}

build_eventlog() {
       zgrep -i "quboleallocation\|Total resources requested\|Added Node\|ExecutorAllocationManager" LOG_${1}  |grep -v "memory:0" > EVENTLOG_${1}
}

main ()
{
	for i in "$@"
	do
		echo "******************************    ${i} ****************************************"
		parselog ${i}
		summary ${i}
		build_eventlog ${i}
		echo
		echo
	done
}


main $@
